import { NatengPage } from './app.po';

describe('nateng App', function() {
  let page: NatengPage;

  beforeEach(() => {
    page = new NatengPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
