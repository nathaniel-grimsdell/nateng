import {Component, OnInit} from '@angular/core';
import {RouteInterceptorService} from './service/routernterceptor.service';

import {ActivatedRoute, Router} from '@angular/router';


const DEFAULT_PAGE_HEADER = 'Welcome!';

@Component({
  selector: 'app-thing',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private pageHeader: string = DEFAULT_PAGE_HEADER;
  private includeBreadcrumb: boolean;

  constructor(private routeInterceptorService: RouteInterceptorService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.routeInterceptorService.getRouteOnNavigationEnd(route => {
      if (!route) {
        return;
      }
      if (!route.snapshot) {
        return;
      }
      if (!route.snapshot.data || route.snapshot.data.length < 1) {
        return;
      }


      // only show on the programme dashboard route
      this.includeBreadcrumb = (this.router.url && this.router.url.startsWith('/home'));

    });
  }
}

