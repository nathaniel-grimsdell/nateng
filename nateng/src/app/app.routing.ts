import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {AboutComponent } from './about/about.component';
import {HomeComponent } from './home/home.component';
import {ProfileComponent} from './profile/profile.component';
import {NavComponent} from './nav/nav.component';


const ROUTES: Routes = [

    {
      path: 'about',
      component: AboutComponent
    },

    {
      path: 'Nav',
      component: NavComponent
    },

    {
      path: 'home',
      component: HomeComponent
    },

    {
      path: 'profile',
      component: ProfileComponent
    },

];

export const routing = RouterModule.forRoot(ROUTES);
@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES)
  ],
  exports: [
    RouterModule
  ],
  providers: [

  ]
})
export class AppRoutingModule {
}

